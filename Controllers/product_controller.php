<?php 
	/**
	* Description: Controller  Product
	* Author: Misserine Carvajal
	* Date: 23-05-2019
	*/
	
	require_once('message_controller.php');
	require_once('image_controller.php');

	class ProductController
	{	
		public $msj;
	
		public function __construct() 
		{
			
		}

		public function index()
		{	
			if ($this->msj){
				var_dump('entro aqui');
			}

			$products = product::all();
			require_once('Views/Product/index.php');
		}

		public function add()
		{	
			$msj='';
			if (isset($_POST['action']) && $_POST['action'] === 'add') 
			{
				$product = new Product(null, $_POST['sku'], $_POST['name'], $_POST['price'], $_POST['description'], $_POST['quantity'], null); 
				
				$msg = new MessageController();

				if (Product::save($product)){
					$msj = $msg->mess('The product was successfully registered!',0);
				}else{
					$msj = $msg->mess('Error: the product was not successfully registered.',1);
				}
				
				header('Location: ?controller=product&action=index');
			}else{

				$newSku = Product::getNextSku();
				require_once('Views/Product/add.php');
			}			
		}

		public function uploadImagen($id, $imgfile, $image = null)
		{
			if (isset($imgfile) && !empty($imgfile)) {
				$oImage = new ImageController();
				$imgAnt = $image; //imagen anterior

				//validamos el nombre de la imagen
				$imgUploaded = $oImage->slugUploadImgRed($imgfile['name']);
				$destino = "includes/images/products/";

				//guardamos la imagen en la bd
				if (Product::updateImage($id, $imgUploaded)){
					//redimensionar la foto
					if ($oImage->uploadImgRed($imgfile, $destino, 150, 150)) {
					    //eliminar foto anterior del directorio si existe
					    if (!empty($imgAnt) && ($imgAnt!= $imgUploaded)){
					    	$respuesta = unlink('/'.$destino.$imgAnt);	
					    }
					    echo 'The image was successfully uploaded.';
					    header('Location: ?controller=product&action=edit&id='.$id);
					}	
				}					    
			}		
		}

		public function edit(){
			//$msj = '';
			if (isset($_POST['action']) && $_POST['action'] === 'edit') {

				if (isset($_FILES["imgfile"]["name"]) && !empty($_FILES["imgfile"]["name"])){
					
					$this->uploadImagen($_POST['id'], $_FILES["imgfile"], $_POST['image']);
				}else{
					$product = new Product($_POST['id'], $_POST['sku'], $_POST['name'], $_POST['price'], $_POST['description'], $_POST['quantity'], $_POST['image']);
				
					$msg = new MessageController();

					if (Product::update($product)){
						$this->msj = $msg->mess('The product was successfully updated!',0);
					}else{
						$this->msj = $msg->mess('Error: the product was not successfully updated.',1);
					}
					header('Location: ?controller=product&action=index');
				}					
			}else{
				if ($_GET['id']){
					$product=Product::getById($_GET['id']);
 				}			
				require_once('Views/Product/edit.html');
			}
		}
		
		
		public function delete()
		{
			$msj = '';
			if (isset($_POST['id'])){

				$msg = new MessageController();
				
				if (product::delete($_POST['id'])){
					$msj = $msg->mess('The product was successfully eliminated!',0);
				}else{
					$msj = $msg->mess('Error: the product was not successfully eliminated.',1);
				}
				header('Location: ?controller=product&action=index');
			}
		}

		public function uploadFile(){
			$msj = '';
			$m;
			if (isset($_FILES["fileToUpload"]["name"])){
				$msg = new MessageController();

				$target_dir = "includes/files/";
				$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
				$uploadOk = 1;
				$fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

				if ($fileType === 'csv'){

					// Check if file already exists
					if (file_exists($target_file)) {
						$msj = $msg->mess("Sorry, file already exists. ", 1);
					    $uploadOk = 0;
					}

					// Check file size
					if ($_FILES["fileToUpload"]["size"] > 500000) {
					    $msj = $msg->mess("Sorry, your file is too large. Your file size is " . $_FILES["fileToUpload"]["size"] .'. ', 1);

					    $uploadOk = 0;
					}

					// Check if $uploadOk is set to 0 by an error
					if ($uploadOk == 0) {
						$msj = $msg->mess("Sorry, your file was not uploaded.", 1);
	
					// if everything is ok, try to upload file
					} else {

					    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
					    	$msj = $msg->mess("The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.", 0);
					        //handle and store data from file
        					if(($handle = fopen($target_file, 'r')) !== FALSE){
        					    set_time_limit(0);
        					    $fila = 0;
        					    $proFileDet = array();
        				    	
        				    	require_once('Models/category.php');				
        				    	require_once('Models/product_category.php');				

        				        while (($datos = fgetcsv($handle, 100000, ";")) !== FALSE) { 
        				            if ($fila > 0) {	
        				                $proFileDet[$fila]['nome'] = $datos[0];
        				                $proFileDet[$fila]['sku'] = $datos[1];
        				                $proFileDet[$fila]['descricao'] = $datos[2];
        				                $proFileDet[$fila]['quantidade'] = $datos[3];
        				                $proFileDet[$fila]['preco'] = $datos[4];
        				                $proFileDet[$fila]['categoria'] = $datos[5];   
        				                $proFileDet[$fila]['nro'] = $fila; 

        				                //if dosn't exist then, save the product
        				                $product = Product::getBySKU($datos[1]);
        				                if ($product->id == null){
											$product = new Product(null, $datos[1], $datos[0], $datos[4], $datos[2], $datos[3], null); 

											//save and get the last id product inserted
											$idProduct = Product::save($product);
        				                }else{
        				                	$idProduct = $product->id;
        				                }        				                

										//explode categories
										$arrCat = array_map('strtolower', explode('|', $datos[5]));
										
										$max = sizeof($arrCat);
										for($i = 0; $i < $max; $i++)
										{
											if ($arrCat[$i] !== null){
												//validar si existe, si no guardarla en categories
												$category = Category::getByName($arrCat[$i]);
												if ($category->id == null){//no existe
													$category->id = null;
													$category->name = $arrCat[$i];
													
													//insertar categoria
													$idCategory = Category::save($category);	
												}else{//si existe
													$idCategory = $category->id;
												}

												//validar si no existe, se guarda
												$procat = ProductCategory::getById($idProduct,$idCategory);
												if ($procat->product_id == null){
													$procat = new ProductCategory($idProduct,$idCategory);
													$result = ProductCategory::save($procat);		
												}
											}
										}
	   					            }
    					            $fila++;    
    					        }
    					        fclose($handle);
        					}
					    } else {
   							$msj = $msg->mess("Sorry, there was an error uploading your file. ", 1);
					    }
					}
				}else{
					$msj = $msg->mess("Error: Not valid extension.", 1);
				}
			}
			require_once('Views/Product/uploadFile.html');	
		}
	}
