<?php

class ImageController
{

    /**
     * Default configuration.
     *
     * @var array
     */
    //protected $_defaultConfig = [];

    public function uploadImgRed($file, $destino, $width, $height){
  	
    	$this->verExtensionImg($file, $destino, $width, $height);

        return true;
    }

    public function verExtensionImg($file, $destino, $width, $height){
    	extract($file);

        $nname = $this->slugUploadImgRed($name);
    	switch ($type) {
    		case 'image/jpeg';
    		case 'image/pjpeg';
    			$image = imagecreatefromjpeg($tmp_name);
    			$imgRedimens = $this->redimensImg($image, $width, $height);
    			imagejpeg($imgRedimens, $destino . $nname);

    			break;
    		case 'image/png';
    		case 'image/x-png';
    			$image = imagecreatefrompng($tmp_name);
    			$imgRedimens = $this->redimensImg($image, $width, $height);
    			imagepng($imgRedimens, $destino . $nname);

    			break;
    	}
    }

    protected function redimensImg($image, $width, $height) {
    	$width_orig = imagesx($image);
    	$height_orig = imagesy($image);

    	$imgRedimens = imagecreatetruecolor(($width), $height);

    	imagecopyresampled($imgRedimens, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

    	return $imgRedimens;
    } 

    /*
    protected function crearDirectorioImgRed($destino){
        $directorio = new Folder($destino);

        if (is_null($directorio->path)){
            $directorio->create($destino);
        }
    }*/

    protected function upload($file, $destino){
    	extract($file);
    	
        if (move_uploaded_file($tmp_name, $destino . $name)){
    		return $name;
    	}else{
    		return false;
    	}
    }

    public function slugUploadImgRed($name){
    	$formato['a'] = 'ÀÁÄÃÅÂâàáãäÆÈÉÊËÊèéëêÌÍĨĪĬÎíìïîÒÓÕÔóòôöÛÙÚÜùúûüÇç"?!/(){}[]<>+-_;:*,#$%&¨@¹²³£¢¬§ªº=\\\'';
    	$formato['b'] = 'aaaaaaaaaaaaeeeeeeeeeiiiiiiiiiioooooooouuuuuuuuCc                                       ';
    	$name = strtr(utf8_decode($name), utf8_decode($formato['a']), $formato['b']);
    	$name = str_replace(' ','-', $name);
    	$name = str_replace(['-----','----','---','--'], '-', $name);
    	$name = strtolower($name);

    	return $name;

    }
}
