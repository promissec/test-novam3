<?php 
	/**
	* Description: Controlador da entidade Welcome
	* Author: Misserine Carvajal
	* Date: 22-05-2019
	*/
	class WelcomeController
	{	
		public function __construct(){}

		public function index(){
			require_once('Models/user.php');				
			$usuCount = User::getCount();
			$users = User::allLimit();
			
			require_once('Models/category.php');				
			$catCount = Category::getCount();	
			$categories = Category::allLimit();	

			require_once('Models/product.php');				
			$proCount = Product::getCount();
			$products = Product::allLimit();		

			require_once('Views/Welcome/index.php');
		}

		public function error(){
			require_once('Views/Welcome/error.html');
		}



	}
