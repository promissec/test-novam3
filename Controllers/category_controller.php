<?php 
	/**
	* Description: Controlador da entidade Category
	* Author: Misserine Carvajal
	* Date: 22-05-2019
	*/
	
	require_once('message_controller.php');

	class CategoryController
	{	

		public function __construct()
		{
		
		}

		public function index()
		{	
			$categories=Category::all();
			require_once('Views/Category/index.php');
		}

		public function add()
		{
			if (isset($_GET['action']) && $_GET['action'] === 'add') 
			{
				$category = new Category(null, $_POST['name']);

				$msg = new MessageController();
			
				if (Category::save($category)){
					$msg->mess('The category was successfully registered!',0);
				}else{
					$msg->mess('Error: the category was not successfully registered.',1);
				}
				header('Location: ?controller=category&action=index');
			}
		}

		//actualizar
		public function edit()
		{
			if (isset($_GET['action']) && $_GET['action'] === 'edit') 
			{
				$category = new Category($_POST['id'], $_POST['name']);	

				$msg = new MessageController();

				if (Category::update($category)){
					$msg->mess('The category was successfully updated!',0);
				}else{
					$msg->mess('Error: the category was not successfully updated.',1);
				}

				header('Location: ?controller=category&action=index');
			}
		}

		public function delete()
		{
			if (isset($_POST['id']))
			{
				$msg = new MessageController();

				if (Category::delete($_POST['id'])){
					$msg->mess('The category was successfully eliminated!',0);
				}else{
					$msg->mess('Error: the category was not successfully eliminated.',1);
				}
				header('Location: ?controller=category&action=index');
			}		
		}
		
	}

