<?php 
	/**
	* Description: Controlador da entidade Usuario
	* Author: Misserine Carvajal
	* Date: 22-05-2019
	*/
	require_once('message_controller.php');

	class UserController
	{	
		public $msj;

		public function __construct(){}

		public function index(){

			if ($this->msj){
				echo $this->msj;
			}

			$users = User::all();
			require_once('Views/User/index.php');
		}

		public function add(){

			if (isset($_POST['action']) && $_POST['action'] === 'add') 
			{
				$user = new User(null,$_POST['name'],$_POST['email']);

				$msg = new MessageController();

				if (User::save($user)){
					$this->msj = $msg->mess('The user was successfully registered!',0);
				}else{
					$this->msj = $msg->mess('Error: the user was not successfully registered.',1);
				}
				
				header('Location: ?controller=user&action=index');
			}else{
				require_once('Views/User/add.php');
			}		
		}

		public function edit(){

			if (isset($_POST['action']) && $_POST['action'] === 'edit') 
			{				
				$user = new User($_POST['id'],$_POST['name'],$_POST['email']);

				$msg = new MessageController();

				if (User::update($user)){
					$this->msj = $msg->mess('The user was successfully updated!',0);
				}else{
					$this->msj = $msg->mess('Error: the user was not successfully updated.',1);
				}

				header('Location: ?controller=user&action=index');
			}else{
				if ($_GET['id']){
					$user=User::getById($_GET['id']);
 				}			
				require_once('Views/User/edit.php');
			}
		}
		

		public function delete()
		{
			if (isset($_GET['id'])){

				$msg = new MessageController();
				
				if (User::delete($_GET['id'])){
					$this->msj = $msg->mess('The user was successfully eliminated!',0);
				}else{
					$this->msj = $msg->mess('Error: the user was not successfully eliminated.',1);
				}
				header('Location: ?controller=user&action=index');
			}
		}
		
		public function view()
		{	
			$user = User::getById($_GET['id']);
			require_once('Views/User/view.php');
		}
		
	}
