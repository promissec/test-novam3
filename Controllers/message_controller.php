<?php

class MessageController
{

	public function __construct(){} 

	public function success($msg){
		setcookie('message',$msg);
		setcookie('class','alert alert-success text-center',time()+20);
	}

	public function error($msg){
		setcookie('message',$msg);
		setcookie('class','alert alert-danger text-center',time()+20);
	}


	public function mess($msjs, $class = 0){

			$mensaje = "<div class='alert alert-success text-center' role='alert'  id='alert-js'>
						$msjs
					    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
							<span aria-hidden='true'>&times;</span>
						</button>
					</div>";
			if ($class == 1) {
				$mensaje = "<div class='alert alert-danger text-center' role='alert'  id='alert-js'>
						$msjs
					    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
							<span aria-hidden='true'>&times;</span>
						</button>
					</div>";	
			}
			
			return $mensaje;
		}

}