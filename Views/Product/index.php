<div class="d-flex">
     <div class="mr-auto p-2">
        <h2>Products List </h2>
    </div>
    <div class="p-2">    
        <a href="?controller=welcome&action=index" class="link-float-back" >
            <img src="includes/images/min/icon-back2.svg" alt=""></a>
        
        <a href="?controller=product&action=add" class="link-float " ><img src="includes/images/min/icon-add-plus.svg" alt=""> </a>
    </div>
</div>
<hr>

<div class="table-responsive">
    <table class="table table-system" cellpadding="15px">
        <thead>
            <tr>
                <td>Id</td>
                <td>SKU</td>
                <td>Name</td>
                <td>Price</td>
                <td>Complement</td>
                <td>Quantity</td>
                <td>Actions</td>
            </tr>
        </thead>
        <tbody>
    <?php foreach ($products as $product) { ?>
            <tr>
                <td><?= $product->id ?></td>
                <td><?= $product->sku ?></td>
                <td><?= $product->name ?></td>
                <td><?= $product->price ?></td>
                <td><?= $product->description ?></td>
                <td><?= $product->quantity ?></td>
                <td class="box-buttons-table">
                    <a data-toggle="modal" data-target="#multiModal" onclick='javascript:liftModal("view",<?= $product->id ?>,"<?= $product->sku ?>","<?= $product->name ?>","<?= $product->price ?>","<?= $product->description ?>","<?= $product->quantity ?>","<?= $product->image ?>");' class="link-button-table">
                       <img src="includes/images/min/icon-view.svg" alt="view" title="View Product">
                    </a>
                    <a href="?controller=product&action=edit&id=<?= $product->id ?>" class="link-button-table " >
                        <img src="includes/images/min/icon-edit3.svg" alt="edit" title="Edit Product">
                    </a>   
                    <a data-toggle="modal" data-target="#multiModal" onclick='javascript:liftModal("delete",<?= $product->id ?>,"<?= $product->sku ?>","<?= $product->name ?>","<?= $product->price ?>","<?= $product->description ?>","<?= $product->quantity ?>","<?= $product->image ?>");' class="link-button-table" id="delete">
                       <img src="includes/images/min/icon-delete2.svg" alt="delete" title="Delete Product">
                    </a>
                </td>
            </tr>
    <?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <td>Id</td>
                <td>SKU</td>
                <td>Name</td>
                <td>Price</td>
                <td>Complement</td>
                <td>Quantity</td>
                <td>Actions</td>
            </tr>
        </tfoot>
    </table>
</div>

<!--inicio multiModal -->
<div class="modal fade bd-example-modal-lg" id="multiModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #2E4051;color: white;">
                <h4 class="modal-title" name="modalLabel" id="modalLabel"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="formSend" action="?controller=product&action=delete">
                    <div id="container" style="display: flex; width: 100%;">
                        <div id="frm" style="width: 70%;">                       
                            <dl class="row">
                                <dt class="col-lg-3">ID</dt>
                                <dd class="col-lg-8"><input class="col-lg-8" type="text" name="id" id="idPro" readonly></dd>

                                <dt class="col-lg-3">SKU</dt>
                                <dd class="col-lg-8">
                                    <div class="d-flex">
                                        <input class="col-lg-4" type="text" name="sku" id="sku" readonly>
                                        <div style="margin-left: 10px;">
                                            <img src="" id="imgSku">
                                        </div>
                                    </div>
                                </dd>

                                <dt class="col-lg-3">Product name</dt>
                                <dd class="col-lg-8"><input class="col-lg-8" type="text" name="name" id="name" readonly></dd>

                                <dt class="col-lg-3">Price</dt>
                                <dd class="col-lg-8"><input class="col-lg-8" type="text" name="price" id="price" readonly></dd>

                                <dt class="col-lg-3">Complement</dt>
                                <dd class="col-lg-8"><input class="col-lg-8" type="text" name="description" id="description" readonly></dd>

                                <dt class="col-lg-3">Quantity</dt>
                                <dd class="col-lg-8"><input class="col-lg-8" type="text" name="quantity" id="quantity" readonly>
                                    <input class="col-lg-8" type="text" name="image" id="n-image" readonly></dd>
                            </dl>                
                        </div>
                        <div id="imgDiv" style="margin-top: 20px;margin-right: 30px;">
                            <div class="text-center"> 
                                <div style="height: 180px; width: 180px;vertical-align: center;">
                                    <img src="" id="idImage" alt="Avatar" style="border: 4px solid #f2f2f2;width:100%; height:100%" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <span id="spanDelete" class="text-danger"></span>
                    </div>
                    <div class="modal-footer text-center">
                        <div id="divBotones"> 
                            <input id="btnDelete" type="submit" class="btn btn-success btn-ok-edit" value="Accept" >
                            <button id="btnCancel" type="button" class="btn btn-secondary" data-dismiss="modal" >Cancel</button>
                        </div>
                    </div>
                </form>
            </div>      
        </div>
    </div>
</div>

<script>
    liftModal = function(tmodal, id, sku, name, price, description, quantity, image){
        $('#idPro').val(id);
        $('#sku').val(sku);
        $('#name').val(name);
        $('#price').val(price);
        $('#description').val(description);
        $('#quantity').val(quantity);
        $('#nimage').val(image);
        $('#btnCancel').text('Cancel');
        $("#btnDelete").css('visibility', 'hidden');
        $("#spanDelete").css('visibility', 'hidden');
        $('#divBotones').css('margin-left', '0');
        $('#divBotones').css('margin-right', '0');
        
        //to show image
        var ruta;
        var tit;
        ruta = "includes/images/min/carrinho.svg";
        tit = "Add your image";
        if (image){
            ruta = "includes/images/products/"+image;
            tit = "Edit your image";
        }
        $("#idImage").attr("src",ruta);
        $("#aImg").prop("title", tit);
        
        //to show image sku
        $("#imgSku").attr("src", "includes/barcode-php1/generar.php?sku="+sku+"&w=1.8");

        //quitar apariencia de textbox
        $('dl > dd')
           .find("input")
           .each(function(index, element) {
              $(element).css('background-color','transparent');
              $(element).css('border-color','transparent');
              $(element).prop('readonly',true);

           });        

        switch (tmodal) {
            case 'view':
                $('#modalLabel').text('View Product #' + id);
                $('#btnDelete').css('visibility', 'hidden');
                $('#btnCancel').text('Close');
                
                break;
            case 'delete':
                $('#modalLabel').text('Delete Product #' + id);
                $('#btnDelete').css('visibility', 'visible');
                $('#spanDelete').css('visibility', 'visible');
                $('#spanDelete').text('Do you really want to delete product #' + id + '?');
                $('#divBotones').css('margin-left', 'auto');
                $('#divBotones').css('margin-right', 'auto');
        }     


    };
</script>