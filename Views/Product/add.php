<div class="d-flex">
     <div class="mr-auto p-2">
        <h2>Register Product </h2>
    </div>
    <div class="p-2">
    	<a href="?controller=product&action=index" class="link-float-back" >
            <img src="includes/images/min/icon-back2.svg" alt="">
        </a>
    </div>
</div>
<hr>

<form action='?controller=product&action=add' method='post'>
	<input type='hidden' name='action' value='add'>
	<div class="form-row">
	    <div class="form-group col-md-8">
	        <label><span class="text-danger">*</span> SKU (New code)</label>
	        <div class="d-flex">
	        	<input type='text' id="idSku" name='sku' class='form-control col-sm-3 ' placeholder='SKU' required readonly value="<?=$newSku ?>">
	        	<div style="margin-left: 10px;">
	        		<img src="includes/barcode-php1/generar.php?sku=<?=$newSku ?>&w=2">
	        	</div>     	
	        </div>
	    </div>
	</div>
	<div class="form-row">
	    <div class="form-group col-md-8">
	        <label><span class="text-danger">*</span> Product Name</label>
	        <input type='text' name='name' class='form-control' placeholder='Product Name' autofocus required>
	    </div>
	</div>
	<div class="form-row">
	    <div class="form-group col-md-8">
	        <label><span class="text-danger">*</span> Price</label>
	        <input type='text' name='price' class='form-control' placeholder='Price' required>
	    </div>
	</div>	
	<div class="form-row">
	    <div class="form-group col-md-8">
	        <label> Complement</label>
	        <input type='text' name='description' class='form-control' placeholder='Complement' >
	    </div>
	</div>	
	<div class="form-row">
	    <div class="form-group col-md-8">
	        <label><span class="text-danger">*</span> Quantity</label>
	        <input type='number' name='quantity' class='form-control' placeholder='Quantity' required>
	    </div>
	</div>	
	<input type='submit' class="btn btn-success" value='Save'>
</form>

<script type="text/javascript">
	$(document).ready(function() {
    $('#category_id').multiselect();
});
</script>