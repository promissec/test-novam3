<div class="d-flex">
     <div class="mr-auto p-2">
        <h2>Edit User #<?= $user->id?> </h2>
    </div>
    <div class="p-2">    
        <a href="?controller=user&action=index" class="link-float-back" >
            <img src="includes/images/min/icon-back2.svg" alt=""></a>
    </div>
</div>
<hr>

<form action='?controller=user&action=edit' method='post'>
	<input type='hidden' name='action' value='edit'>
	<input type='hidden' name='id' value='<?= $user->id?>'>
	<div class="form-row">
	    <div class="form-group col-md-6">
	        <label><span class="text-danger">*</span> Name</label>
	        <input type='text' name='name' value="<?= $user->name?>" class='form-control' placeholder='Full Name' required>
	    </div>
	</div>
	<div class="form-row">
	    <div class="form-group col-md-6">
	        <label><span class="text-danger">*</span> E-mail</label>
	        <input type='email' name='email' value="<?= $user->email?>" class='form-control' placeholder='Put your best email' required>
	    </div>
	</div>
	
	<input type='submit' class="btn btn-success" value='Save'>
</form>