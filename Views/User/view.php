<div class="d-flex">
     <div class="mr-auto p-2">
        <h2>Users List </h2>
    </div>
    <div class="p-2">    
        <a href="?controller=user&action=index" class="link-float-back" >
            <img src="includes/images/min/icon-back2.svg" alt=""></a>
    </div>
</div>
<hr>
<div id="frm" style="width: 70%;">                       
    <dl class="row">
        <dt class="col-lg-3">ID</dt>
        <dd class="col-lg-8"><?= $user->id?></dd>

        <dt class="col-lg-3">Name</dt>
        <dd class="col-lg-8"><?= $user->name ?></dd>

        <dt class="col-lg-3">E-mail</dt>
        <dd class="col-lg-8"><?= $user->email?></dd>
    </dl>                
</div>
