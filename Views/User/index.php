<div class="d-flex">
     <div class="mr-auto p-2">
        <h2>Users List </h2>
    </div>
    <div class="p-2">    
        <a href="?controller=welcome&action=index" class="link-float-back" >
            <img src="includes/images/min/icon-back2.svg" alt=""></a>
        <a href="?controller=user&action=add" onclick="javascript:liftModal('add','','')" class="link-float " ><img src="includes/images/min/icon-add-plus.svg" alt=""> </a>
    </div>
</div>
<hr>
<table class="table-system" cellpadding="15px">
    <thead>
        <tr>
            <td>Id</td>
            <td>Name</td>
            <td>Email</td>
            <td>Actions</td>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($users as $user) { ?>
        <tr>
            <td><?= $user->id ?></td>
            <td><?= $user->name ?></td>
            <td><?= $user->email ?></td>
            <td class="box-buttons-table">
                <a href="?controller=user&action=view&id=<?=$user->id ?>" class="link-button-table" id="desable">
                    <img src="includes/images/min/icon-view.svg" alt="view" title="View User">
                </a>
                <a href="?controller=user&action=edit&id=<?=$user->id ?>" class="link-button-table" id="active">
                    <img src="includes/images/min/icon-edit3.svg" alt="edit" title="Edit User">
                </a>                
                <a href="?controller=user&action=delete&id=<?=$user->id ?>" class="link-button-table" id="delete">
                    <img src="includes/images/min/icon-delete2.svg" alt="delete" title="Delete User">
                </a>
            </td>
        </tr>
    <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td>Id</td>
            <td>Name</td>
            <td>Email</td>
            <td>Actions</td>
        </tr>
    </tfoot>
</table>
