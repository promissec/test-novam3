<div class="d-flex">
     <div class="mr-auto p-2">
        <h2>Categories List </h2>
    </div>
    <div class="p-2">    
        <a href="?controller=welcome&action=index" class="link-float-back" >
            <img src="includes/images/min/icon-back2.svg" alt=""></a>
        <a data-toggle="modal" data-target="#multiModal"  data-whatever="Add Category" onclick="javascript:liftModal('add', '', '');" class="link-float " ><img src="includes/images/min/icon-add-plus.svg" alt=""> </a>
    </div>
</div>
<hr>
<div class="table-responsive">
    <table class="table table-system" cellpadding="15px">
        <thead>
            <tr>
                <td>Id</td>
                <td>Category</td>
                <td>Actions</td>
            </tr>
        </thead>
        <tbody>
    <?php foreach ($categories as $category) { ?>
            <tr>
                <td><?= $category->id ?></td>
                <td><?= $category->name ?></td>
                <td class="box-buttons-table">
                <a data-toggle="modal" data-target="#multiModal"  data-whatever="Add Category" onclick='javascript:liftModal("view",<?= $category->id ?>,"<?= $category->name ?>");' class="link-button-table">
                   <img src="includes/images/min/icon-view.svg" alt="view" title="View Category">
                </a>

                <a data-toggle="modal" data-target="#multiModal"  data-whatever="Add Category" onclick='javascript:liftModal("edit",<?= $category->id ?>,"<?= $category->name ?>");' class="link-button-table " >
                    <img src="includes/images/min/icon-edit3.svg" alt="edit" title="Edit Category">
                </a>   

                <a data-toggle="modal" data-target="#multiModal"  data-whatever="Delete Category" onclick='javascript:liftModal("delete",<?= $category->id ?>,"<?= $category->name ?>");' class="link-button-table" id="delete">
                   <img src="includes/images/min/icon-delete2.svg" alt="delete" title="Delete Category">
                </a>
                </td>
            </tr>
    <?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <td>Id</td>
                <td>Category</td>
                <td>Actions</td>
            </tr>
        </tfoot>
    </table>
</div>

<!--inicio multiModal -->
<div class="modal fade" id="multiModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #2E4051;color: white;">
                <h4 class="modal-title" name="modalLabel" id="modalLabel">Category</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="formSend" action="?controller=category&action=add">
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label col-form-label-sm">
                            <span class="text-danger">*</span> ID
                        </label>
                        <div class="col-md-5">
                            <input name="id" id='idCat' class='form-control form-control-sm' readonly>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label col-form-label-sm">
                            <span class="text-danger">*</span> Category name
                        </label>
                        <div class="col-md-5">
                            <input name='name' id='name' data-inline="true" class='form-control form-control-sm' required>
                        </div>
                    </div>
                    <span id="spanDelete" class="text-center text-danger"></span>
                    <div class="modal-footer text-center">
                        <div id="divBotones"> 
                            <input id="btnDelete" type="submit" onclick="javascript:if($('#idCat').val() !== ''){$('#formSend').attr('action','?controller=category&action=delete');};" class="btn btn-success btn-ok-edit" value="Accept" >
                            <input id="btnSubmit" type="submit" onclick="javascript:if($('#idCat').val()!==''){$('#formSend').attr('action','?controller=category&action=edit');};" class="btn btn-success btn-ok-edit" value="Save" >
                            <button id="btnCancel" type="button" class="btn btn-secondary" data-dismiss="modal" >Cancel</button>
                        </div>
                    </div>
                </form>
            </div>      
        </div>
    </div>
</div>

<!--Fin multiModal-->
<script>
    liftModal = function(tmodal, id, name){
        $('#idCat').val(id);
        $('#name').val(name);
        $('#name').prop('readonly',false);
        $("#btnSubmit").css('visibility', 'visible');
        $('#btnCancel').text('Cancel');
        $("#btnDelete").css('visibility', 'hidden');
        $("#spanDelete").css('visibility', 'hidden');
        $('#divBotones').css('margin-left', '0');
        $('#divBotones').css('margin-right', '0');

        switch (tmodal) {
            case 'add':
                $('#modalLabel').text('Add Category');
                $('#name').focus();
                break;
            case 'edit':
                $('#modalLabel').text('Edit Category #' + id);
                break;
            case 'view':
                $('#modalLabel').text('View Category #' + id);
                $('#name').prop('readonly',true);
                $('#btnSubmit').css('visibility', 'hidden');
                $('#btnCancel').text('Close');
                
                break;
            case 'delete':
                $('#name').prop('readonly',true);
                $('#modalLabel').text('Delete Category #' + id);
                $('#btnSubmit').css('visibility', 'hidden');
                $('#btnDelete').css('visibility', 'visible');
                $('#spanDelete').css('visibility', 'visible');
                $('#spanDelete').text('Do you really want to delete category #' + id + '?');
                $('#divBotones').css('margin-left', 'auto');
                $('#divBotones').css('margin-right', 'auto');

        }     
    };
</script>
