<html>
<head>
	<title>Test Developer PHP</title>
		<meta name="author" content="Misserine C.">
		<meta name="description" content="Test de Nova M3">
		<meta name="keywords" content="Nova M3, Test, Software, TI, Emprego">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<!-- Css -->
		<link rel="stylesheet" href="includes/css/app.css">
		<link rel="stylesheet" href="bootstrap-select.min.css">
		<link rel="stylesheet" href="bootstrap.min.css">
		
		<!-- View Port -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<style type="text/css">
			a:link{
				text-decoration:none;
			}
		</style>
</head>
<body>
	<!-- Include menu -->
	<?php require_once('includes/menu/menu.html'); ?>
		<div class="main">
			<div class="wrap push content p-2">
			<?php require_once('routes.php'); ?> 
		</div>
	</div>

	<footer></footer>
	<script src="includes/js/app.js"></script>  
	<script src="includes/js/jquery-3.3.1.min.js"></script>
	<script src="includes/js/bootstrap.min.js"></script>
	<script src="includes/js/bootstrap-select.min.js"></script>
</body>
</html>