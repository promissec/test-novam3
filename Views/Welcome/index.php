<div class="box-units">    
    <ul>
        <li>
            <a href="?controller=user&action=index">
                <div class="d-flex align-items-center mb10">
                    <img src="includes/images/min/icon-user.svg" alt="">
                    <span class="name-units">Users</span>
                </div>
                <span class="number-units"><?= $usuCount ?></span>
            </a>
        </li>
        <li class="diviser-list"></li>
        <li>
            <a href="?controller=category&action=index">
                <div class="d-flex align-items-center mb10">
                    <img src="includes/images/min/icon-category.svg" alt="">
                    <span class="name-units">Categories</span>
                </div>
                <span class="number-units"><?= $catCount ?></span>
            </a>
        </li>
        <li class="diviser-list"></li>
        <li>
            <a href="?controller=product&action=index">
                <div class="d-flex align-items-center mb10">
                    <img src="includes/images/min/icon-cart.svg" alt="">
                    <span class="name-units">Products</span>
                </div>
                <span class="number-units"><?= $proCount ?></span>
            </a>
        </li>
        <li class="diviser-list"></li>
        <!--li>
            <a href="javascript:;" class="add-button">
                <div class="d-flex align-items-center mb10">
                    <img src="includes/images/min/icon-add-plus.svg" alt="">
                    <span class="name-units">Add button</span>
                </div>
                <span class="number-units">9.999</span> -->
            <!--/a>
        </li>
        <li class="diviser-list"></li-->
    </ul>
</div>
<div class="row">
    <div class="col-12">
        <div class="box-dashboard">
            <h1 class="title-dashboard" title="Full list of Users"><a href="?controller=user&action=index">Users</a></h1>
            <div class="diviser-dashboard"></div>
                <div class="row">
                
                <?php foreach ($users as $user): ?>
                    <div class="col-12 col-lg-4">
                        <div class="box-user">
                            <div class="image-user" style="background-image: url('includes/images/min/user.svg');">
                                <img src="includes/images/min/user.svg" alt="">
                            </div>
                            <strong class="name-user"><?=$user->name?></strong>
                            <p class="function-user"><?=$user->email?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 col-lg-6">
        <div class="box-dashboard">
            <h1 class="title-dashboard" title="Full list of Categorys"><a href="?controller=category&action=index">Categorys</a></h1>
            <div class="diviser-dashboard"></div>
            <table class="table-system" cellpadding="15px">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Category</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($categories as $category): ?>
                    <tr>
                        <td><?= $category->id ?></td>
                        <td><?= $category->name ?></td>

                        <td class="box-buttons-table">
                            <a href="?controller=category&action=index" class="link-button-table">
                               <img src="includes/images/min/icon-view.svg" alt="view" title="View Category">
                            </a>
                            <a href="?controller=category&action=index" class="link-button-table " >
                                <img src="includes/images/min/icon-edit3.svg" alt="edit" title="Edit Category">
                            </a>
                            <a href="?controller=category&action=index" class="link-button-table" id="delete">
                               <img src="includes/images/min/icon-delete2.svg" alt="delete" title="Delete Category">
                            </a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td>Id</td>
                        <td>Category</td>
                        <td>Actions</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    
    <div class="col-12 col-lg-12">
        <div class="box-dashboard">
            <h1 class="title-dashboard" title="Full list of Products"><a href="?controller=product&action=index">Products</a></h1>
            <div class="diviser-dashboard"></div>
            <table class="table-system" cellpadding="15px">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>SKU</td>
                        <td>Name</td>
                        <td>Price</td>
                        <td>Complement</td>
                        <td>Quantity</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($products as $product): ?>
                    <tr>
                        <td><?= $product->id ?></td>
                        <td><?= $product->sku ?></td>
                        <td><?= $product->name ?></td>
                        <td><?= $product->price ?></td>
                        <td><?= $product->description ?></td>
                        <td><?= $product->quantity ?></td>
                        <td class="box-buttons-table">
                            <a href="?controller=product&action=index" class="link-button-table ">
                               <img src="includes/images/min/icon-view.svg" alt="view" title="View Product">
                            </a>
                            <a href="?controller=product&action=edit&id=<?= $product->id ?>" class="link-button-table " >
                                <img src="includes/images/min/icon-edit3.svg" alt="edit" title="Edit Product">
                            </a>   
                            <a href="?controller=product&action=index" class="link-button-table ">
                               <img src="includes/images/min/icon-delete2.svg" alt="delete" title="Delete Product">
                            </a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td>Id</td>
                        <td>SKU</td>
                        <td>Name</td>
                        <td>Price</td>
                        <td>Complement</td>
                        <td>Quantity</td>
                        <td>Actions</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>