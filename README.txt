
* A seguir as ferramentas usadas durante o desenvolvimento do projeto

XAMPP for Windows 7.3.1
PHP Version 7.3.1
Apache/2.4.37
phpMyAdmin 4.8.4
Servidor do banco de dados MariaDB 10.1.37
Cliente de base de dados: libmysql - mysqlnd 5.0.12-dev - 20150407

* As informações de conexão ao banco de dados são especificadas no arquivo 'connection.php'.

* Para executar o projeto é necessário adicioná-lo a um servidor (xampp, wamp, etc).
Execute o arquivo fornecido 'test_nova.sql' para criar o banco de dados, colocando suas informações de usuário e senha.
