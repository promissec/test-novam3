<?php
/*****************************************************************************************
* Author: Agustin Ramos
* Evilnapsis
* Description: Crear código de Barras o Barcodes con PHP
* WebSite: https://evilnapsis.com/2018/02/26/crear-codigo-de-barras-o-barcodes-con-php/
*****************************************************************************************/

require_once('class/BCGFontFile.php');
require_once('class/BCGColor.php');
require_once('class/BCGDrawing.php');

require_once('class/BCGcode128.barcode.php');
header('Content-Type: image/png');

$w = 2;
$csku = '0000-0000';
if (isset($_GET['w'])){
	$w=$_GET['w'];
}
if (isset($_GET['sku'])){
	$csku=$_GET['sku'];
}

$colorFront = new BCGColor(0, 0, 0);
$colorBack = new BCGColor(255, 255, 255);

$code = new BCGcode128();
$code->setScale($w);
$code->setThickness(20);
$code->setForegroundColor($colorFront);
$code->setBackgroundColor($colorBack);
$code->parse($csku);

$drawing = new BCGDrawing('', $colorBack);
$drawing->setBarcode($code);

$drawing->draw();
$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
?>