<?php

	//función que llama al controlador y su respectiva acción, que son pasados como parámetros
	function call($controller, $action){
		//importa el controlador desde la carpeta Controllers
		require_once('Controllers/' . $controller . '_controller.php');
		//crea el controlador
		switch($controller){
			case 'user':
				require_once('Models/user.php');
				$controller = new UserController();
				break; 
			case 'product':
				require_once('Models/product.php');
				$controller = new ProductController();
				break; 
			case 'category':
				require_once('Models/category.php');
				$controller = new CategoryController();
				break; 
			case 'welcome':
				//require_once('Models/usuario.php');
				$controller = new WelcomeController();
				break; 
		}
		//llama a la acción del controlador
		$controller->{$action }();
	}

	//array con los controladores y sus respectivas acciones
	$controllers= array(
						'user'=>['index','view','add','edit','delete'],
						'product'=>['index','view','add','edit','delete','uploadFile', 'uploadImagen'],
						'category'=>['index','view','add','edit','delete'],
						'welcome'=>['index','error']
						);
	//verifica que el controlador enviado desde index.php esté dentro del arreglo controllers
	if (array_key_exists($controller, $controllers)) {
		//verifica que el arreglo controllers con la clave que es la variable controller del index exista la acción
		if (in_array($action, $controllers[$controller])) {
			//llama  la función call y le pasa el controlador a llamar y la acción (método) que está dentro del controlador
			call($controller, $action);
		}else{
			call('welcome', 'error');
		}
	}else{// le pasa el nombre del controlador y la pagina de error
		call('welcome', 'error');
	}
