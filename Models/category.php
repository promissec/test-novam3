<?php
/**
* Model of Category
* Author: Misserine C.
* Date: 22-05-2019
*/
class Category
{
	//atributos
	public $id;
	public $name;
	public $count;

	//constructor de la clase
	function __construct($id, $name)
	{
		$this->id=$id;
		$this->name=$name;
	}

	//función para obtener todas las categorias
	public static function all(){
		$listaCategories =[];
		$db=Db::getConnect();
		$sql=$db->query('SELECT * FROM categories');

		// carga en la $listaCategories cada registro desde la base de datos
		foreach ($sql->fetchAll() as $category) {
			$listaCategories[]= new Category($category['id'],$category['name']);
		}
		return $listaCategories;
	}

	public static function allLimit(){
		$listaCategories =[];
		$db=Db::getConnect();
		$sql=$db->query('SELECT * FROM categories LIMIT 6');

		// carga en la $listaCategories cada registro desde la base de datos
		foreach ($sql->fetchAll() as $category) {
			$listaCategories[]= new Category($category['id'],$category['name']);
		}
		return $listaCategories;
	}

	//la función para registrar un Category
	public static function save($category){
			$db=Db::getConnect();
			$insert=$db->prepare('INSERT INTO categories VALUES(NULL,:name)');
			$insert->bindValue('name',$category->name);
			$insert->execute();
			return $db->lastInsertId();
		}

	//la función para actualizar 
	public static function update($category){
		$db=Db::getConnect();
		$update=$db->prepare('UPDATE categories SET name=:name WHERE id=:id');
		$update->bindValue('id',$category->id);
		$update->bindValue('name',$category->name);
		return $update->execute();
	}

	// la función para eliminar por el id
	public static function delete($id){
		$db=Db::getConnect();
		$delete=$db->prepare('DELETE FROM categories WHERE ID=:id');
		$delete->bindValue('id',$id);
		return $delete->execute();
	}

	//la función para obtener un Category por el id
	public static function getById($id){
		//buscar
		$db=Db::getConnect();
		$select=$db->prepare('SELECT * FROM categories WHERE ID=:id');
		$select->bindValue('id',$id);
		$select->execute();
		//asignarlo al objeto Category
		$categoryDb=$select->fetch();
		$category= new Category($categoryDb['id'],$categoryDb['name']);
		return $category;
	}

	//la función para obtener un Category por el id
	public static function getByName($name){
		//buscar
		$db=Db::getConnect();
		$select=$db->prepare('SELECT * FROM categories WHERE LOWER(name)=LOWER(:name)');
		$select->bindValue('name',$name);
		$select->execute();
		//asignarlo al objeto Category
		$categoryDb=$select->fetch();
		$category= new Category($categoryDb['id'],$categoryDb['name']);
		return $category;
	}

	public static function getCount(){
		//buscar
		$db=Db::getConnect();
		$select=$db->prepare('SELECT count(*) as count FROM categories');
		$select->execute();
		//asignarlo al objeto Category
		$categoryDb=$select->fetch();
		
		$category = $categoryDb['count'];
		return $category;	
	}
}
?>