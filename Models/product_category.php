<?php
/**
* Model of Product_category
* Author: Misserine C.
* Date: 26-05-2019
*/
class ProductCategory
{
	//atributos
	public $product_id;
	public $category_id;
	public $count;

	//constructor de la clase
	function __construct($product_id, $category_id)
	{
		$this->product_id=$product_id;
		$this->category_id=$category_id;
	}

	//función para obtener todas las categorias
	public static function all(){
		$listaProCats =[];
		$db=Db::getConnect();
		$sql=$db->query('SELECT * FROM product_categories');

		// carga en la $listaCategories cada registro desde la base de datos
		foreach ($sql->fetchAll() as $procat) {
			$listaProCats[]= new ProductCategory($procat['product_id'],$procat['category_id']);
		}
		return $listaProCats;
	}

	//la función para registrar un Category
	public static function save($procat){
		$db=Db::getConnect();
		$insert=$db->prepare('INSERT INTO product_categories VALUES(:product_id,:category_id)');
		$insert->bindValue('product_id',$procat->product_id);
		$insert->bindValue('category_id',$procat->category_id);
		return  $insert->execute();
	}

	// la función para eliminar por el id
	public static function delete($product_id, $category_id){
		$db=Db::getConnect();
		$delete=$db->prepare('DELETE FROM product_categories WHERE product_id=:product_id AND category_id=:category_id');
		$delete->bindValue('product_id',$product_id);
		$delete->bindValue('category_id',$category_id);
		return $delete->execute();
	}

	//la función para obtener un Category por el id
	public static function getById($product_id, $category_id){
		//buscar
		$db=Db::getConnect();
		$select=$db->prepare('SELECT * FROM product_categories WHERE product_id=:product_id AND category_id=:category_id');
		$select->bindValue('product_id',$product_id);
		$select->bindValue('category_id',$category_id);
		$select->execute();
		//asignarlo al objeto Category
		$procatDb=$select->fetch();
		$procat= new ProductCategory($procatDb['product_id'],$procatDb['category_id']);
		return $procat;
	}

/*
	public static function getCount(){
		//buscar
		$db=Db::getConnect();
		$select=$db->prepare('SELECT count(*) as count FROM categories');
		$select->execute();
		//asignarlo al objeto Category
		$categoryDb=$select->fetch();
		
		$category = $categoryDb['count'];
		return $category;	
	}
*/
}
