<?php
/**
* Model of Product
* Author: Misserine C.
* Date: 23-05-2019
**/
class product
{

	//atributos
	public $id;
	public $sku;
	public $name;
	public $price;
	public $description;
	public $quantity;
	public $image;
	public $count;
	public $newSku;

	//constructor de la clase
	function __construct($id, $sku, $name, $price, $description, $quantity, $image)
	{
		$this->id=$id;
		$this->sku=$sku;
		$this->name=$name;
		$this->price=$price;
		$this->description=$description;
		$this->quantity=$quantity;
		$this->image=$image;
	}

	//función para obtener todas las categorias
	public static function all()
	{
		$listaproducts =[];
		$db=Db::getConnect();
		$sql=$db->query('SELECT * FROM products');

		// carga en la $listaproducts cada registro desde la base de datos
		foreach ($sql->fetchAll() as $product) {
			$listaproducts[]= new product($product['id'], $product['sku'], $product['name'], $product['price'], $product['description'], $product['quantity'], $product['image']);
		}
		return $listaproducts;
	}

	public static function allLimit()
	{
		$listaproducts =[];
		$db=Db::getConnect();
		$sql=$db->query('SELECT * FROM products LIMIT 6');

		// carga en la $listaproducts cada registro desde la base de datos
		foreach ($sql->fetchAll() as $product) {
			$listaproducts[]= new product($product['id'], $product['sku'], $product['name'], $product['price'], $product['description'], $product['quantity'], $product['image']);
		}
		return $listaproducts;
	}

	//la función para registrar un product
	public static function save($product)
	{
		$db=Db::getConnect();
		$insert=$db->prepare('INSERT INTO products VALUES(NULL,:sku,:name, :price, :description, :quantity, :image)');
		$insert->bindValue('sku',$product->sku);
		$insert->bindValue('name',$product->name);
		$insert->bindValue('price',$product->price);
		$insert->bindValue('description',$product->description);
		$insert->bindValue('quantity',$product->quantity);
		$insert->bindValue('image',$product->image);
		$insert->execute();
		return $db->lastInsertId();
	}

	//la función para actualizar 
	public static function update($product)
	{
		$db=Db::getConnect();
		$update=$db->prepare('UPDATE products SET sku=:sku, name=:name, price=:price, description=:description, quantity=:quantity, image=:image WHERE id=:id');
		
		$update->bindValue('id',$product->id);
		$update->bindValue('sku',$product->sku);
		$update->bindValue('name',$product->name);
		$update->bindValue('price',$product->price);
		$update->bindValue('description',$product->description);
		$update->bindValue('quantity',$product->quantity);
		$update->bindValue('image',$product->image);
		return $update->execute();
	}

	public static function updateImage($id, $image)
	{
		$db=Db::getConnect();
		$update=$db->prepare('UPDATE products SET image=:image WHERE id=:id');
		
		$update->bindValue('id',$id);
		$update->bindValue('image',$image);
		return $update->execute();
	}

	// la función para eliminar por el id
	public static function delete($id)
	{
		$db=Db::getConnect();
		$delete=$db->prepare('DELETE FROM products WHERE ID=:id');
		$delete->bindValue('id',$id);
		return $delete->execute();
	}

	//la función para obtener un product por el id
	public static function getById($id)
	{
		//buscar
		$db=Db::getConnect();
		$select=$db->prepare('SELECT * FROM products WHERE ID=:id');
		$select->bindValue('id',$id);
		$select->execute();
		//asignarlo al objeto product
		$productDb = $select->fetch();
		$product = new product($productDb['id'],$productDb['sku'],$productDb['name'],$productDb['price'],$productDb['description'],$productDb['quantity'],$productDb['image']);
		return $product;
	}

	//la función para obtener un product por el id
	public static function getBySKU($sku)
	{
		//buscar
		$db=Db::getConnect();
		$select=$db->prepare('SELECT * FROM products WHERE sku=:sku');
		$select->bindValue('sku',$sku);
		$select->execute();
		//asignarlo al objeto product
		$productDb = $select->fetch();
		$product = new product($productDb['id'],$productDb['sku'],$productDb['name'],$productDb['price'],$productDb['description'],$productDb['quantity'],$productDb['image']);
		return $product;
	}

	public static function getCount()
	{
		//buscar
		$db=Db::getConnect();
		$select=$db->prepare('SELECT count(*) as count FROM products');
		$select->execute();
		//asignarlo al objeto product
		$productDb=$select->fetch();
		
		$product = $productDb['count'];
		return $product;	
	}

	public static function getNextSku(){
		$db=Db::getConnect();
		$select=$db->prepare("SELECT CASE WHEN MAX(sku) IS NULL THEN '0000-0001' ELSE concat(substr(lpad(max(replace(sku,'-',''))+1,'8',0),1,4),'-',substr(lpad(max(replace(sku,'-',''))+1,'8',0),-4)) END AS newSku FROM products order by 1 desc limit 1");
		$select->execute();
		//asignarlo al objeto product
		$productDb=$select->fetch();
		
		$product = $productDb['newSku'];
		return $product;	
	}

}
