<?php
/**
* Modelo para el acceso a la base de datos y funciones CRUD
* Autor: ELivar Largo
* Sitio Web: wwww.ecodeup.com
*/
class User
{
	//atributos
	public $id;
	public $name;
	public $email;
	public $count;

	//constructor de la clase
	function __construct($id, $name, $email)
	{
		$this->id=$id;
		$this->name=$name;
		$this->email=$email;
	}

	//función para obtener todos los usuarios
	public static function all(){
		$listaUsuarios =[];
		$db=Db::getConnect();
		$sql=$db->query('SELECT * FROM users');

		// carga en la $listaUsuarios cada registro desde la base de datos
		foreach ($sql->fetchAll() as $user) {
			$listaUsuarios[]= new User($user['id'],$user['name'], $user['email']);
		}
		return $listaUsuarios;
	}

	public static function allLimit(){
		$listaUsuarios =[];
		$db=Db::getConnect();
		$sql=$db->query('SELECT * FROM users LIMIT 6');

		// carga en la $listaUsuarios cada registro desde la base de datos
		foreach ($sql->fetchAll() as $user) {
			$listaUsuarios[]= new User($user['id'],$user['name'], $user['email']);
		}
		return $listaUsuarios;
	}

	//la función para registrar un usuario
	public static function save($user){
			$db=Db::getConnect();
			$insert=$db->prepare('INSERT INTO users VALUES(NULL,:name,:email)');
			$insert->bindValue('name',$user->name);
			$insert->bindValue('email',$user->email);
			$insert->execute();
		}

	//la función para actualizar 
	public static function update($user){
		$db=Db::getConnect();
		$update=$db->prepare('UPDATE users SET name=:name, email=:email WHERE id=:id');
		$update->bindValue('id',$user->id);
		$update->bindValue('name',$user->name);
		$update->bindValue('email',$user->email);
		$update->execute();
	}

	// la función para eliminar por el id
	public static function delete($id){
		$db=Db::getConnect();
		$delete=$db->prepare('DELETE FROM users WHERE ID=:id');
		$delete->bindValue('id',$id);
		$delete->execute();
	}

	//la función para obtener un usuario por el id
	public static function getById($id){
		//buscar
		$db=Db::getConnect();
		$select=$db->prepare('SELECT * FROM users WHERE ID=:id');
		$select->bindValue('id',$id);
		$select->execute();
		//asignarlo al objeto usuario
		$userDb=$select->fetch();
		$user= new User($userDb['id'],$userDb['name'],$userDb['email']);
		return $user;
	}

	public static function getCount(){
		//buscar
		$db=Db::getConnect();
		$select=$db->prepare('SELECT count(*) as count FROM users');
		$select->execute();
		//asignarlo al objeto usuario
		$userDb=$select->fetch();
		
		$user = $userDb['count'];
		return $user;	
	}
}
?>