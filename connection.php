<?php
	/**
	* Connection to the data base
	* Autor: Misserine C.
	* Date: 22-05-2019
	*/
	class Db
	{
		private static $instance=NULL;
		
		private function __construct(){}

		private function __clone(){}
		
		public static function getConnect(){
			if (!isset(self::$instance)) {
				$pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
				self::$instance= new PDO('mysql:host=localhost;dbname=test_nova','root','',$pdo_options);
			}
			return self::$instance;
		}
	}
?>